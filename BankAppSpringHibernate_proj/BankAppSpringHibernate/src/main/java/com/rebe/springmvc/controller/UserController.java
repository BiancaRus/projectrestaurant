package com.rebe.springmvc.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.rebe.springmvc.model.User;
import com.rebe.springmvc.service.UserService;

@Controller
@RequestMapping("/")
public class UserController {
	@Autowired
	private UserService userService;
	@Autowired
	MessageSource messageSource;

	@RequestMapping(value = "/usersList", method = RequestMethod.GET)
	public ModelAndView list(Model model, HttpServletRequest request) {
		if(request.getSession().getAttribute("blabla")!=null && !request.getSession().getAttribute("blabla").equals(""))
		{ModelAndView modelAndView = new ModelAndView("usersView");
		List<User> allEmployeedUsers = userService.getEmployees();

		modelAndView.addObject("employees", allEmployeedUsers);

		return modelAndView;}
		ModelAndView modelAndView = new ModelAndView("redirect:/loginFailed");

		return modelAndView;
	}

	@RequestMapping(value = "/registerUser", method = RequestMethod.GET)
	public ModelAndView showUserForm() {
		ModelAndView modelAndView = new ModelAndView("addUser");

		modelAndView.addObject("user", new User());

		return modelAndView;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String saveUser(Model model, @ModelAttribute @Valid User user, BindingResult result) {

		ModelAndView modelAndView = new ModelAndView("redirect:/usersList");
		if (result.hasErrors() || !userService.isUnique(user.getUsername())) {
			return "addUser";
		}
		// user.setRole(2);
		userService.addUser(user);
		String message = "User was successfully added.";
		modelAndView.addObject("message", message);

		return "redirect:/usersList";
	}

	@RequestMapping(value = "/delete/{iduser}", method = RequestMethod.GET)
	public ModelAndView deleteUser(@PathVariable int iduser) {

		ModelAndView modelAndView = new ModelAndView("redirect:/usersList");
		userService.deleteUser(iduser);
		String message = "User was successfully deleted.";
		modelAndView.addObject("message", message);
		return modelAndView;
	}

	@RequestMapping(value = "/edit/{iduser}", method = RequestMethod.GET)
	public ModelAndView updateUser(@PathVariable int iduser) {
		ModelAndView modelAndView = new ModelAndView("updateUser");
		User user = userService.getUserById(iduser);
		modelAndView.addObject("user", user);
		return modelAndView;
	}

	@RequestMapping(value = "/edit/{iduser}", method = RequestMethod.POST)
	public String updateUser(@ModelAttribute("user") @Valid User user, BindingResult result, @PathVariable int iduser,
			ModelMap model) {
		if (result.hasErrors()) {
			user.setUserId(iduser);
			model.addAttribute("user", user);
			return "updateUser";
		}
		user.setUserId(iduser);
		userService.updateUser(user);
		return "redirect:/usersList";
	}

}
