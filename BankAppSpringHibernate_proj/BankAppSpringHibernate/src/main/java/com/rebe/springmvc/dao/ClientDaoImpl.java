package com.rebe.springmvc.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;

import com.rebe.springmvc.model.Client;

@Repository("clientDao")
public class ClientDaoImpl extends AbstractDao<Integer, Client> implements ClientDao {

	public void addClient(Client client) {
		persist(client);
	}

	public void updateClient(Client client) {

		Client clientToUpdate = getByKey(client.getClientId());

		clientToUpdate.setFirstName(client.getFirstName());
		clientToUpdate.setLastName(client.getLastName());
		clientToUpdate.setEmail(client.getEmail());
		clientToUpdate.setIdentityCardNumber(client.getIdentityCardNumber());
		clientToUpdate.setPersonalNumericCode(client.getPersonalNumericCode());
		clientToUpdate.setAddress(client.getAddress());
		clientToUpdate.setPhone(client.getPhone());
		clientToUpdate.setEmail(client.getEmail());
		clientToUpdate.setSum(client.getSum());
		update(clientToUpdate);

	}

	public Client getClient(int clientId) {
		return getByKey(clientId);
	}

	public void deleteClient(int clientId) {
		Client client = getByKey(clientId);
		delete(client);
	}

	@SuppressWarnings("unchecked")
	public List<Client> getClients() {
		Criteria criteria = createEntityCriteria();
		return (List<Client>) criteria.list();
	}

	public boolean isUnique(String cnp) {
		for(Client client: getClients()){
			if(client.getPersonalNumericCode().equalsIgnoreCase(cnp))
				return false;
		}
		return true;
	}
	
	

}
