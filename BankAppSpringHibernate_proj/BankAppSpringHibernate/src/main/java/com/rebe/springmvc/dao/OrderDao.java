package com.rebe.springmvc.dao;

import java.util.List;

import com.rebe.springmvc.model.Order;

public interface OrderDao {
	public void addConsultation(Order consultation);

	public List<Order> getAllConsultations();

	public List<Order> getConsultationByPatient(int patient);
	public void updateConsultation(Order consultation);
	public void deleteConsultation(int id);
	
	public Order getConsultationById(int id);
}
