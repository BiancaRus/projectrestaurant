package com.rebe.springmvc.dao;

import java.util.List;

import com.rebe.springmvc.model.User;

public interface UserDao {
	public void addUser(User user);

	public void updateUser(User user);

	public void deleteUser(int id);

	public User getUserById(int id);

	public List<User> getUsers();

	public List<User> getEmployees();

	public boolean isUnique(String username);
}
