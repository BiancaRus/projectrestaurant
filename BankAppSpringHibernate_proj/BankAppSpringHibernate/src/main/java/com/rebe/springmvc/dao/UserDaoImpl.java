package com.rebe.springmvc.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;

import com.rebe.springmvc.dao.UserDao;
import com.rebe.springmvc.model.User;

@Repository("userDao")
public class UserDaoImpl extends AbstractDao<Integer, User> implements UserDao {

	public void addUser(User user) {
		persist(user);

	}

	public void updateUser(User user) {
		User userToUpdate = getUserById(user.getUserId());

		userToUpdate.setRole(user.getRole());
		userToUpdate.setUsername(user.getUsername());
		userToUpdate.setUserFirstName(user.getUserFirstName());
		userToUpdate.setPassword(user.getPassword());
		userToUpdate.setUserLastName(user.getUserLastName());
		userToUpdate.setUserEmail(user.getUserEmail());

		update(userToUpdate);

	}

	public User getUserById(int id) {
		return getByKey(id);
	}

	public void deleteUser(int id) {
		delete(getByKey(id));
	}

	@SuppressWarnings("unchecked")
	public List<User> getUsers() {
		Criteria criteria = createEntityCriteria();
		return (List<User>) criteria.list();

	}

	@SuppressWarnings("unchecked")
	public List<User> getEmployees() {
		Criteria criteria = createEntityCriteria();
		return (List<User>) criteria.list();
	}

	public boolean isUnique(String username) {
		for (User user : getUsers()) {
			if (user.getUsername().equalsIgnoreCase(username))
				return false;
		}
		return true;
	}

}
