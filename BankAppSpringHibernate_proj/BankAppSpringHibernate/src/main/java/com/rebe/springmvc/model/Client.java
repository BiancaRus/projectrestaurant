package com.rebe.springmvc.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;

@Entity
@Table(name = "client")
public class Client {

	@Id
	@GeneratedValue
	@Column(name = "clientid")
	private int clientId;

	@Column(name = "first_name")
	@NotNull
	private String firstName;

	@Column(name = "last_name")
	@NotNull
	private String lastName;

	@Column(name = "identity_card_number")
	@Size(min = 3, message = "required")
	private String identityCardNumber;

	@Column(name = "pnc", nullable = false)
	@Size(min = 13, max = 13)
	private String personalNumericCode;

	@Column(name = "address", nullable = false)
	@NotNull
	private String address;

	@Column(name = "phone_number")
	@NotNull
	@Size(min = 10, max = 10)
	private String phone;

	@Column(name = "email", nullable = false)
	@NotNull
	@Email
	private String email;

	@Column(name = "birth_date", nullable = false)
	private Date birth_date;
	
	@Column(name = "sum_in_account")
	private int sum;

	public int getSum() {
		return sum;
	}

	public void setSum(int sum) {
		this.sum = sum;
	}

	public Date getBirth_date() {
		return birth_date;
	}

	public void setBirth_date(Date birth_date) {
		this.birth_date = birth_date;
	}

	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getIdentityCardNumber() {
		return identityCardNumber;
	}

	public void setIdentityCardNumber(String identityCardNumber) {
		this.identityCardNumber = identityCardNumber;
	}

	public String getPersonalNumericCode() {
		return personalNumericCode;
	}

	public void setPersonalNumericCode(String personalNumericCode) {
		this.personalNumericCode = personalNumericCode;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
