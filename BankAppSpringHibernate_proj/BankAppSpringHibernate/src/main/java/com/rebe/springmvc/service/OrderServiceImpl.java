package com.rebe.springmvc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rebe.springmvc.dao.OrderDao;
import com.rebe.springmvc.model.Order;

@Service("orderService")
@Transactional
public class OrderServiceImpl implements ConsultationService {

	@Autowired
	private OrderDao dao;

	public void addConsultation(Order consultation) {
		dao.addConsultation(consultation);
	}

	public List<Order> getAllConsultations() {
		return dao.getAllConsultations();
	}

	public List<Order> getConsultationByPatient(int patient) {
		return dao.getConsultationByPatient(patient);
	}

	public void updateConsultation(Order consultation) {
		dao.updateConsultation(consultation);

	}

	public void deleteConsultation(int id) {
		dao.deleteConsultation(id);
	}

	public Order getConsultationById(int id) {
		return dao.getConsultationById(id);
	}
}