package com.rebe.springmvc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rebe.springmvc.dao.UserDao;
import com.rebe.springmvc.model.User;
import com.rebe.springmvc.service.UserService;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDAO;

	public void addUser(User user) {
		userDAO.addUser(user);

	}

	public void updateUser(User user) {
		userDAO.updateUser(user);
	}

	public User getUserById(int id) {
		return userDAO.getUserById(id);
	}

	public void deleteUser(int id) {
		userDAO.deleteUser(id);
	}

	public List<User> getUsers() {
		return userDAO.getUsers();
	}

	public List<User> getEmployees() {
		return userDAO.getEmployees();
	}

	public boolean isUnique(String username) {
		return userDAO.isUnique(username);
	}

}
