<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style type="text/css">
ul {
	list-style-type: none;
	margin: 0;
	padding: 10px;
	width: 200px;
	background-color: #f1f1f1;
}

li a {
	display: block;
	color: #000;
	padding: 8px 0 8px 16px;
	text-decoration: none;
}

li a:hover {
	background-color: #555;
	color: white;
}

h2 {
	color: #505050;
	font-family: sans-comic;
	font-size: 300%;
}

h1 {
	color: #505050;
	font-family: sans-comic;
	font-size: 200%;
	paddingRight: 10px;
}

body {
	padding: 20px;
	background-color: #C0C0C0;
}

input[type=text] {
	padding: 5px;
	border: 2px solid #ccc;
	-webkit-border-radius: 5px;
	border-radius: 5px;
	color: white;
}

input[type=text]:focus {
	border-color: #333;
}

input[type=submit] {
	padding: 5px 15px;
	background: #900000;
	border: 0 none;
	color: white;
	cursor: pointer;
	-webkit-border-radius: 5px;
	border-radius: 5px;
}
</style>
<title>AmbasadorKitchenManagement</title>
</head>

<body bgcolor="	#808080">
	<center>
		<h2>Welcome Kitchen Master ${user.userFirstName } ${user.userLastName }!</h2>
	</center>
	<ul>
		<li><a href="/SpringHibernateExample/">Log Out</a></li>
	</ul>
	<center>
		<h2>Ambasador Orders List</h2>

		<table id="id01" border="4" cellpadding="20" bordercolor=" #400000 ">
				<tr>
				<td>Client Name</td>
				<td>Order Description</td>
				<td>Food & Quantity</td>
				<td>Table Number</td>
				<td>Order Date</td>
				<td>Order is Served!</td>
			</tr>

			<c:forEach var="consultation" items="${consultations}">
				<tr>
					<td><c:out
							value="${consultation.patient.firstName}  ${consultation.patient.lastName}" /></td>
					<td><c:out value="${consultation.description}" /></td>
					<td><c:out value="${consultation.diagnosis}" /></td>
					<td><c:out value="${consultation.tableNumber}" /></td>
					<td><c:out value="${consultation.date} " /></td>
					<td><a
						href="/SpringHibernateExample/serveOrder/${consultation.consultationId}">Served</a></td>
				</tr>

			</c:forEach>
		</table>
		<br>
	</center>
	<p id="pp0" class="b02"></p>
	<script>
		var myTimer = window.setInterval(scanner, 1000);
		var xmlhttp = new XMLHttpRequest();

		var url = "http://localhost:8080/SpringHibernateExample/greeting";
		var patientTable = document.getElementById('id01');
		var infoBox = document.getElementById('pp0');
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				var myArr = JSON.parse(xmlhttp.responseText);
				myFunction(myArr);
			}
		};
		scanner();

		function scanner() {
			xmlhttp.open("GET", url, true);
			xmlhttp.send();
		}

		function myFunction(patients) {
			console.log(patients);
			patientTable.innerHTML = '';
			var out = "<tr>	<td>Client Name</td><td>Order Description</td><td>Food & Quantity</td><td>Table Number</td>	<td>Order Date</td><td>Order is Served!</td>	</tr>";

			var s = '<tr>	<td>Client Name</td><td>Order Description</td><td>Food & Quantity</td><td>Table Number</td>	<td>Order Date</td><td>Order is Served!</td>	</tr>';
			var i = 0;
			console.log("the length is " + patients.length);
		
			for (i = 0; i < patients.length; i++) {
				s = s + '<tr> <td>' + patients[i].patient.firstName
						+ '</td> <td>' + patients[i].description
						+ ' </td> <td>' + patients[i].diagnosis 
						+ ' </td> <td>' + patients[i].tableNumber 
						+ ' </td> <td>' + patients[i].date
						+ ' </td>';
				s = s
						+ '<td><a href="'+ "/SpringHibernateExample/serveOrder/"+patients[i].consultationId+'">'
						+ "Served!" + '</a></td>';
			}

			if (i == 0) {
				infoBox.innerHTML = 'No patients scheduled for you have arrived yet.';
				patientTable.innerHTML = '';
			} else {
				infoBox.innerHTML = '';
				patientTable.innerHTML = patientTable.innerHTML + s;
			}
		}
	</script>


</body>
</html>