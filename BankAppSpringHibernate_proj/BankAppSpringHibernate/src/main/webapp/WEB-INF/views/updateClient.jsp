<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style type="text/css">
ul {
	list-style-type: none;
	margin: 0;
	padding: 10px;
	width: 200px;
	background-color: #f1f1f1;
}

li a {
	display: block;
	color: #000;
	padding: 8px 0 8px 16px;
	text-decoration: none;
}

li a:hover {
	background-color: #555;
	color: white;
}

h2 {
	color: #505050;
	font-family: sans-comic;
	font-size: 300%;
}

h1 {
	color: #505050;
	font-family: sans-comic;
	font-size: 200%;
	paddingRight: 10px;
}

body {
	padding: 20px;
	background-color: #C0C0C0;
}

input[type=text] {
	padding: 5px;
	border: 2px solid #ccc;
	-webkit-border-radius: 5px;
	border-radius: 5px;
}

input[type=text]:focus {
	border-color: white;
}

input[type=submit] {
	padding: 5px 15px;
	background: #900000;
	border: 0 none;
	cursor: pointer;
	color: white;
	-webkit-border-radius: 5px;
	border-radius: 5px;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>AmbasadorUpdateClient</title>
</head>
<body>
	<ul>
		<li><a href="/SpringHibernateExample/clientsList">Ambasador
				Clients</a></li>
		<li><a href="/SpringHibernateExample/billsList">Ambasador
				Orders</a></li>
		<li><a href="/SpringHibernateExample/">Log Out</a></li>
	</ul>
	<center>
		<br> <br>
		<h2>Ambasador Restaurant Update Client</h2>


		<form:form commandName="client"
			action="/SpringHibernateExample/editClient/${client.clientId }"
			method="POST">
			<table>

				<tr>
					<td>First Name</td>
					<td><form:input path="firstName" /></td>
					<td><form:errors path="firstName" cssClass="error" /></td>

				</tr>
				<tr>
					<td>Last Name</td>
					<td><form:input path="lastName" /></td>
					<td><form:errors path="lastName" cssClass="error" /></td>
				</tr>

				<tr>
					<td>Identity Card Number</td>
					<td><form:input path="personalNumericCode" /></td>
					<td><form:errors path="personalNumericCode" cssClass="error" /></td>
				</tr>
				<tr>
					<td>Personal Numeric Code</td>
					<td><form:input path="identityCardNumber" /></td>
					<td><form:errors path="identityCardNumber" cssClass="error" /></td>


				</tr>
				<tr>
					<td>Address</td>
					<td><form:input path="address" /></td>
					<td><form:errors path="address" cssClass="error" /></td>

				</tr>
				<tr>
					<td>Phone</td>
					<td><form:input path="phone" /></td>
					<td><form:errors path="phone" cssClass="error" /></td>

				</tr>
				<tr>
					<td>Email</td>
					<td><form:input path="email" /></td>
					<td><form:errors path="email" cssClass="error" /></td>

				</tr>

				<tr>
					<td></td>
					<td><input type="submit" value="Update" /></td>
				</tr>
			</table>
		</form:form>
	</center>
</body>
</html>