<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style type="text/css">
ul {
	list-style-type: none;
	margin: 0;
	padding: 10px;
	width: 200px;
	background-color: #f1f1f1;
}

li a {
	display: block;
	color: #000;
	padding: 8px 0 8px 16px;
	text-decoration: none;
}

li a:hover {
	background-color: #555;
	color: white;
}

h2 {
	color: #505050;
	font-family: sans-comic;
	font-size: 300%;
}

h1 {
	color: #505050;
	font-family: sans-comic;
	font-size: 200%;
	paddingRight: 10px;
}

body {
	padding: 20px;
	background-color: #C0C0C0;
}

input[type=text] {
	padding: 5px;
	border: 2px solid #ccc;
	-webkit-border-radius: 5px;
	border-radius: 5px;
	color: white;
}

input[type=text]:focus {
	border-color: #333;
}

input[type=submit] {
	padding: 5px 15px;
	background: #900000;
	border: 0 none;
	cursor: pointer;
	color: white;
	-webkit-border-radius: 5px;
	border-radius: 5px;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>AmbasadorEmployees</title>
</head>
<body>

	<ul>
		<li><a href="/SpringHibernateExample/usersList">Ambasador Employees List</a></li>
		<li><a href="/SpringHibernateExample/">Log Out</a></li>
	</ul>

	<center>
		<br> <br>
		<h2>Ambasador Restaurant Staff</h2>
		<br> <br>

		<table border="4" cellpadding="20" bordercolor=" #400000 ">
			<tr>
				<td>Employee username</td>
				<td>Employee First Name</td>
				<td>Employee Last Name</td>
				<td>Employee Email</td>
				<td>Employee Role</td>
				<td>Delete Employee</td>
				<td>Edit Employee</td>
			</tr>


			<c:forEach var="user" items="${employees}">
				<tr>
					<td>${user.username}</td>
					<td>${user.userFirstName}</td>
					<td>${user.userLastName}</td>
					<td>${user.userEmail}</td>
					<td><c:if test="${user.role == 1}">Ambasador Manager</c:if> <c:if
							test="${user.role == 2}">Ambasador Waiter</c:if> <c:if
							test="${user.role == 3}">Ambasador Kitchen Employee</c:if>
					<td><a href="/SpringHibernateExample/delete/${user.userId}">Delete</a></td>
					<td><a href="/SpringHibernateExample/edit/${user.userId}">Edit</a></td>

				</tr>
			</c:forEach>

		</table>

		<br> <input type="submit" value="Hire New Ambasador Employee"
			onclick="window.location.href='/SpringHibernateExample/registerUser/'" />
	</center>

</body>
</html>