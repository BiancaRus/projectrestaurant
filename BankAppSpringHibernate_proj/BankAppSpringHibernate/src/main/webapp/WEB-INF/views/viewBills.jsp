<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style type="text/css">
ul {
	list-style-type: none;
	margin: 0;
	padding: 10px;
	width: 200px;
	background-color: #f1f1f1;
}

li a {
	display: block;
	color: #000;
	padding: 8px 0 8px 16px;
	text-decoration: none;
}

li a:hover {
	background-color: #555;
	color: white;
}

h2 {
	color: #505050;
	font-family: sans-comic;
	font-size: 300%;
}

h1 {
	color: #505050;
	font-family: sans-comic;
	font-size: 200%;
	paddingRight: 10px;
}

body {
	padding: 20px;
	background-color: #C0C0C0;
}

input[type=text] {
	padding: 5px;
	border: 2px solid #ccc;
	-webkit-border-radius: 5px;
	border-radius: 5px;
	color: white;
}

input[type=text]:focus {
	border-color: #333;
}

input[type=submit] {
	padding: 5px 15px;
	background: #900000;
	border: 0 none;
	color: white;
	cursor: pointer;
	-webkit-border-radius: 5px;
	border-radius: 5px;
}
</style>
<title>AmbasadorAdministration</title>
</head>

<body bgcolor="	#808080">

	<ul>
		<li><a href="/SpringHibernateExample/clientsList">Ambasador
				Clients</a></li>
		<li><a href="/SpringHibernateExample/billsList">Ambasador
				Orders</a></li>
		<li><a href="/SpringHibernateExample/">Log Out</a></li>
	</ul>
	<center>
		<h2>Ambasador Restaurant Orders</h2>

		<table border="4" cellpadding="20" bordercolor=" #400000 ">
			<tr>
				<td>Client Name</td>
				<td>Order Description</td>
				<td>Food & Quantity</td>
				<td>Table Number</td>
				<td>Order Date</td>
				<td>Delete Order</td>
				<td>Edit Order</td>
				<td>Process Bill</td>
			</tr>

			<c:forEach var="consultation" items="${consultations}">
				<tr>
					<td><c:out
							value="${consultation.patient.firstName}  ${consultation.patient.lastName}" /></td>
					<td><c:out value="${consultation.description}" /></td>
					<td><c:out value="${consultation.diagnosis}" /></td>
					<td><c:out value="${consultation.tableNumber}" /></td>
					<td><c:out value="${consultation.date} " /></td>
					<td><a
						href="/SpringHibernateExample/deleteConsultation/${consultation.consultationId}">Delete</a></td>
					<td><a
						href="/SpringHibernateExample/editConsultation/${consultation.consultationId}">Edit</a></td>
					<td><a
						href="/SpringHibernateExample/processBill/${consultation.patient.clientId}">Process Bill</a></td>
				</tr>

			</c:forEach>
		</table>
		<br> <input type="submit" value="Take an order"
			onclick="window.location.href='/SpringHibernateExample/createBill/'" />
	</center>
</body>
</html>