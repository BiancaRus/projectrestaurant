<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page session="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style type="text/css">
ul {
	list-style-type: none;
	margin: 0;
	padding: 10px;
	width: 200px;
	background-color: #f1f1f1;
}

li a {
	display: block;
	color: #000;
	padding: 8px 0 8px 16px;
	text-decoration: none;
}

li a:hover {
	background-color: #555;
	color: white;
}

h2 {
	color: #505050;
	font-family: sans-comic;
	font-size: 300%;
}

h1 {
	color: #505050;
	font-family: sans-comic;
	font-size: 200%;
	paddingRight: 10px;
}

body {
	padding: 20px;
	background-color: #C0C0C0;
}

input[type=text] {
	padding: 5px;
	border: 2px solid #ccc;
	-webkit-border-radius: 5px;
	border-radius: 5px;
	color: white;
}

input[type=text]:focus {
	border-color: #333;
}

input[type=submit] {
	padding: 5px 15px;
	background: #900000;
	border: 0 none;
	color: white;
	cursor: pointer;
	-webkit-border-radius: 5px;
	border-radius: 5px;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>AmbasadorOrderManagement</title>
</head>
<body>


	<input type="hidden" name="${_csrf.parameterName}"
		value="${_csrf.token}" />
	<ul>
		<li><a href="/SpringHibernateExample/billsList">Ambasador
				Orders</a></li>
		<li><a href="/SpringHibernateExample/">Log Out</a></li>
	</ul>
	<center>
		<br> <br>
		<h2>Clients List</h2>



		<table border="4" cellpadding="20" bordercolor=" #400000 ">
			<tr>
				<td>Id</td>
				<td>First Name</td>
				<td>Last Name</td>
				<td>Identity Card Number</td>
				<td>Personal Numeric Code</td>
				<td>Birth Date</td>
				<td>Address</td>
				<td>Phone</td>
				<td>Email</td>
				<td>Sum</td>
				<td>Delete User</td>
				<td>Edit User</td>
			</tr>

			<c:forEach var="client" items="${clients}">
				<tr>
					<td><c:out value="${client.clientId}" /></td>
					<td><c:out value="${client.firstName}" /></td>
					<td><c:out value="${client.lastName}" /></td>
					<td><c:out value="${client.identityCardNumber}" /></td>
					<td><c:out value="${client.personalNumericCode}" /></td>
					<td><c:out value="${client.birth_date}" /></td>
					<td><c:out value="${client.address}" /></td>
					<td><c:out value="${client.phone}" /></td>
					<td><c:out value="${client.email}" /></td>
					<td><c:out value="${client.sum}" /></td>
					<td><a class="others"
						href="/SpringHibernateExample/deleteClient/${client.clientId}">Delete</a></td>
					<td><a class="others"
						href="/SpringHibernateExample/editClient/${client.clientId}">Edit</a></td>
				</tr>
			</c:forEach>

		</table>
		<br> <input type="submit" value="Add Client"
			onclick="window.location.href='/SpringHibernateExample/createClient/'" />
	</center>

</body>
</html>